<!-- TITLE: Study -->
<!-- SUBTITLE: All study info -->

# Study
## Goal 
Storing and sharing knowledge. No war, just code and math. 

## Who can provide info? 
Everyone. Github allows you to suggest any improvements using _Pull request_.

# Content
_1st year:_

0. [Linear Algebra](http://innowiki.ru/study/linear-algebra/)
1. [Calculus](http://innowiki.ru/study/calculus/)
2. [Computer Architecture](http://innowiki.ru/study/computer-architecture/)
3. [Discrete Math](http://innowiki.ru/study/discrete-math/)

_2st year:_

0. [Operating Systems](http://innowiki.ru/study/operating-systems)
1. [Philosophy](http://innowiki.ru/study/philosophy)

