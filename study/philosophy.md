<!-- TITLE: Philosophy -->
<!-- SUBTITLE: A quick summary of Philosophy -->

# Lectures
Unfortunately lectures on philosophy were held in Russian language so the following page will contain information in Russian only.
1. [Lecture 1](http://innowiki.ru/study/philosophy/lecture-1)
2. [Lecture 2](http://innowiki.ru/study/philosophy/lecture-2)
3. [Lecture 3](http://innowiki.ru//study/philosophy/lecture-3)
4. [Lecture 4](http://innowiki.ru/study/philosophy/lecture-4)
5. [Lecture 5](http://innowiki.ru/study/philosophy/lecture-5)
6. [Lecture 6](http://innowiki.ru/study/philosophy/lecture-6)
7. [Lecture 7](http://innowiki.ru/study/philosophy/lecture-7)