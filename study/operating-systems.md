<!-- TITLE: Operating Systems -->
<!-- SUBTITLE: Summary of the operating systems course -->

# Operating course
Since it's not possible to support wiki-like structure of this vast topic "operating systems" we will give you summary of the book "Modern Operating Systems" by A. Tanenbaum

[The book](./operating-systems/modern-operating-systems)

To learn more about C programming language you can refer to the [book](https://en.wikipedia.org/wiki/The_C_Programming_Language) from the authors.