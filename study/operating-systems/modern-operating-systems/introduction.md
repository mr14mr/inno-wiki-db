<!-- TITLE: Introduction -->
<!-- SUBTITLE: Summary of the Introduction chapter -->

# Introduction
## What is an operating system?
## History of operating systems
## Computer hardware review 
## The operating system zoo
## Operating system concepts
## System calls
## Operating system structure
## The world according to C