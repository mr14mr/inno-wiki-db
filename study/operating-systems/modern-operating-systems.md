<!-- TITLE: Modern Operating Systems-->
<!-- SUBTITLE: Summary of the book -->

# Modern Operating Systems
## Topics
1. [Introduction](./modern-operating-systems/introduction)
2. [Processes and threads](./modern-operating-systems/processes-and-threads)
3. [Memory management](./modern-operating-systems/memory-management)
4. [File Systems](./modern-operating-systems/file-systems)
5. [Input/Outpiut](./modern-operating-systems/input/output)
6. [Deadlocks](./modern-operating-systems/deadlocks)
7. [Virtualization and cloud](./modern-operating-systems/virtualization-and-cloud) - wasn't present on lectures
8. [Multiple Processor systems](./modern-operating-systems/multiple-processor-systems) - wasn't present on lectures
9. [Security](./modern-operating-systems/security) - wasn't present on lectures