<!-- TITLE: Combinational logic -->
<!-- SUBTITLE: A quick summary of combinational logic -->
# Combinational logic
## Decoder
A decoder is a logic block that has an n-bit input and
2<sup>n</sup> outputs where only one output is asserted for each input
combination.

Example of 3 to 8 decoder:
![Decoder](/uploads/study-computer-architecture/decoder.png "Decoder")
![Decoder Truth Table](/uploads/study-computer-architecture/decoder-truth-table.png "Decoder Truth Table")
Example with gates:
![Decoder Gates](/uploads/study-computer-architecture/decoder-gates.png "Decoder Gates")

## Multiplexers
A multiplexer is a logic block that has 2<sup>n</sup> bit input and n-bit selector
and 1 output which is one of the n inputs based on the selector value 
 
A selector value (or control value) is the control signal
that is used to select one of the input values of a multiplexer as
the output of the multiplexer.

Basically, multiplexers select which value will go through, all others are blocked.

Example of two-input multiplexer on the left and its implementation with
gates on the right:
![Multiplexer Example](/uploads/study-computer-architecture/multiplexer-example.png "Multiplexer Example")

## Demultiplexers
The demultiplexer performs the function of the inverse multiplexer
– it commutes the input signal to the desired output the number
of which is set by the selector. The other outputs are set to 0.

Example of 1-to-4 demux:
![Demux](/uploads/study-computer-architecture/demux.png "Demux")


## Programmable Logic Array
A Programmable Logic Array (PLA) - structured-logic element
composed of a set of inputs and corresponding input complements
and two stages of logic:
 * The first generates product terms of the inputs
 * The second generates sum terms of the product terms.
Therefore, PLAs implement logic functions as a sum of products.
![Pla](/uploads/study-computer-architecture/pla.png "Pla")

PLA is currently not used because they are not fast enough, consumes a lot of voltage and have large number of gates
