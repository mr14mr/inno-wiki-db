<!-- TITLE: Boolean algebra  -->
<!-- SUBTITLE: A quick summary of memory boolean algebra-->
# Boolean Algebra
All arithmetic operations performed with Boolean quantities have
one of two possible outcomes: either 1 or 0.

Boolean algebra could be applied to on-and-off circuits, where all
signals are characterized as either “high” (1) or “low” (0).

high = 1 = asserted = active = true

low = 0 = deasserted = inactive = false


Often a non-zero value is considered to “be evaluated” to true (but
still true is represented by 1)

##### Addition
Boolean algebra by adding two numbers together

|Addition|
|:---:|
|0+0=0|
|0+1=1|
|1+0=1|
|1+1=1|

Boolean addition corresponds to the logical function of an “OR”
gate, as well as to parallel switch contacts.

![Boolean Function Addition](/uploads/study-computer-architecture/boolean-function-addition.png "Boolean Function Addition")

##### Subtraction
There is no such thing as subtraction in the realm
of Boolean mathematics. Subtraction implies the existence of
negative numbers: 5 - 3 is the same thing as 5 + (-3), and in
Boolean algebra negative quantities are forbidden.

##### Multiplication
It is the same as in real-number algebra: anything multiplied by 0 is 0,
and anything multiplied by 1 remains unchanged:

|Multiplication|
|:---:|
|0∗0=0|
|0∗1=0|
|1∗0=0|
|1∗1=1|

Boolean multiplication corresponds to the logical function of an
“AND” gate, as well as to series switch contacts:
![Boolean Function Multiplication](/uploads/study-computer-architecture/boolean-function-multiplication.png "Boolean Function Multiplication")

##### Complement
 Boolean notation uses a bar above the
variable character to denote complementation.

If A = 0 then ¬A = 1 (and vice versa)

Boolean complementation finds equivalency in the form of the
NOT gate, or a normally-closed switch or relay contact.
![Boolean Function Complement](/uploads/study-computer-architecture/boolean-function-complement.png "Boolean Function Complement")

##### Division
There is no such thing as division in Boolean
mathematics, either, since division is really nothing more than
compounded subtraction, in the same way that multiplication is
compounded addition.

## Truth Table
Boolean functions can be fully computed using a truth table
###### Example
f = a * b * c

|a|b|c|f|
|:---:|:---:|:---:|:---:|
|0|0|0|0|
|0|0|1|0|
|0|1|0|0|
|0|1|1|0|
|1|0|0|0|
|1|0|1|0|
|1|1|0|0|
|1|1|1|1|

## Boolean algebra laws
![Boolean Algebra Laws](/uploads/study-computer-architecture/boolean-algebra-laws.png "Boolean Algebra Laws")

## General theorem
Any logical function can be constructed:
 * using AND gates and inverter
 * using OR gates and inverter
 
Therefore any logical function can be made of two levels with a level of or
and a level of and, plus negation

There are two “inverting” gates NOR and NAND and correspond
to inverted OR and inverted AND gates, respectively.

NOR and NAND gates are called universal, since any logic
function can be built using this one gate type

<maybe some images of such occurences>