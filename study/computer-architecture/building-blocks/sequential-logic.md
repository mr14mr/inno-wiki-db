<!-- TITLE: Sequential Logic -->
<!-- SUBTITLE: A quick summary of sequential logic -->
# Sequential Logic

## State element
A memory element that contains a value, the state
of the element.

Clocking is used to update state elements.

The following diagram explains how state elements are updated.
![Clock State](/uploads/study-computer-architecture/clock-state.png "Clock State")

The inputs to a combinational logic block come from a state
element, and the outputs are written into a state element.

Edge-triggered methodology is a clocking scheme in which all state changes occur on a clock edge.

## Latch
Latch is a circuit used to store information.
It can retain an input signal until reset by another signal

### SR Latch
![Sr Latch Symbol](/uploads/study-computer-architecture/sr-latch-symbol.png "Sr Latch Symbol")
SR is for Set-Reset

Stores one bit of state Q, controls what value is being stored with S, R inputs, have invalid state when R=S=1

![Sr Latch](/uploads/study-computer-architecture/sr-latch.png "Sr Latch")
There are 4 cases:
R=1 and S=0, then Q=0 and not Q = 1

S=1 and R=0, then Q=1 and not Q = 0
![Sr Latch First Cases](/uploads/study-computer-architecture/sr-latch-first-cases.png "Sr Latch First Cases")
R=S=0, then Q = Q<sub>prev</sub>

R=S=1, then Q=0 and not Q=0. <b>Invalid State</b>
![Sr Latch Second Cases](/uploads/study-computer-architecture/sr-latch-second-cases.png "Sr Latch Second Cases")

### D Latch
![D Latch Symbol](/uploads/study-computer-architecture/d-latch-symbol.png "D Latch Symbol")
Improvement over SR Latch, that removes invalid state and adds clock input

Has 2 inputs:
1. CLK: controls when the output changes
2. D: the data input

![D Latch](/uploads/study-computer-architecture/d-latch.png "D Latch")
When CLK = 1, D passes through to Q

When CLK = 0, Q holds its previous value

## Flip Flops
Flip flops are fundamental building blocks of digital electronics
systems.

A flip flop stores a single bit (binary digit) of data.

### D Flip Flop
![D Flip Flop Symbol](/uploads/study-computer-architecture/d-flip-flop-symbol.png "D Flip Flop Symbol")
Constructed from two cascaded D-latches, called the
master and the slave. Both are controlled by one CLK signal.

The latch labeled by L1 is called master, and the latch labeled by
L2 is called slave.
![D Flip Flop](/uploads/study-computer-architecture/d-flip-flop.png "D Flip Flop")
It samples D on rising edge of CLK

When CLK rises from 0 to 1, it is stored
at the internal state of D flip-flop and this
state is propagated to the output Q

Otherwise, Q holds its previous value
Q changes only on rising edge of CLK and therefore called edge-triggered

### Enabled Flip Flop
![Enabled Flip Flops](/uploads/study-computer-architecture/enabled-flip-flops.png "Enabled Flip Flops")
A D Flip Flop with additional input (EN) that control when new data (D) is stored
So if
1. EN = 1: D passes through to Q on the clock edge
2. EN = 0: the flip-flop retains its previous state

It enables signal to change the state of a flip-flop only at desired times and limits the power consumption. 

### Resettable Flip Flop
![Resettabe Flip Flop Symbol](/uploads/study-computer-architecture/resettabe-flip-flop-symbol.png "Resettabe Flip Flop Symbol")
A D Flip Flop with additional input (Reset) that forces internal value to 0
* Reset = 1: Q is forced to 0
* Reset = 0: flip-flop behaves as ordinary D flip-flop

![Resettabe Flip Flop](/uploads/study-computer-architecture/resettabe-flip-flop.png "Resettabe Flip Flop")
### Settable Flip Flop
![Settable Flip Flop Symbol](/uploads/study-computer-architecture/settable-flip-flop-symbol.png "Settable Flip Flop Symbol")
A D Flip Flop with additional input (Setet) that forces internal value to 1
* Set = 1: Q is set to 1
* Set = 0: flip-flop behaves as ordinary D flip-flop

## Latches vs Flip Flops
Latches
 * Latches are building blocks of sequential circuits and these can be build from logic gates
 * Latch is level-sensitive. It change the value as input change.
Flip Flops
 * Flip flops are also building blocks of sequential circuits. But, these
can be build from the latches
 * Flip Flop is edge-sensitive and only changes
state when a control signal goes from high to low or low to high

## Registers
An 8-bit register can be constructed from 8 single-bit flip-flops.
![8 Bit Register](/uploads/study-computer-architecture/8-bit-register.png "8 Bit Register")

## Register Files
Register file is, basically, an array of registers. It consists of a set of registers that can be read and
written by supplying a register number to be accessed.

A register file can be implemented with a decoder for each read or
write port and an array of registers built from D flip flops.



A register file with two read ports and one write port has five
inputs and two outputs.
![Register File Symbol](/uploads/study-computer-architecture/register-file-symbol.png "Register File Symbol")

<b>Reading a register in a register file</b>: It does not change any
state, we need only supply a register number as an input, and the
only output will be the data contained in that register.
![Register File Read](/uploads/study-computer-architecture/register-file-read.png "Register File Read")

<b>Writing a register in a register file</b>: we will need three inputs:
a register number, the data to write, and a clock that controls the
writing into the register.

![Register File Write](/uploads/study-computer-architecture/register-file-write.png "Register File Write")