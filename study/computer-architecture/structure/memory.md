<!-- TITLE: Memory -->
<!-- SUBTITLE: A quick summary of memory -->
# Memory

### Main Memory
* Addressed directly –sometimes said “randomly,” hence RAM
* Fast access, no as fast as register, but still fast
* Volatile structure

### Disk Storage
* Slower to access
* Sequential in accessing nature (while randomly accessible)
* Larger capacity
* Permanent storage
