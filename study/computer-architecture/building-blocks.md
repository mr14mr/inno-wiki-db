<!-- TITLE: Building Blocks -->
<!-- SUBTITLE: A quick summary of Building Blocks -->
# Building blocks of the hardware
[Prerequisite topic](./building-blocks/boolean-algebra)

[Combinational logic](./building-blocks/combinational-logic) = no memory elements
[Sequential logic](./building-blocks/sequential-logic) = memory elements

## Other content
* [Memory](./building-blocks/memory)
