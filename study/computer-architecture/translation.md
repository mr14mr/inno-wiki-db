<!-- TITLE: Translation -->
<!-- SUBTITLE: A quick summary of translation -->
## Translation hierarchy
![Translation Hierarchy](/uploads/study-computer-architecture/translation-hierarchy.png "Translation Hierarchy")

* [Compiler](./translation/compiler)
* [Assembler](./translation/assembler)
* [Linker](./translation/linker)
* [Loader](./translation/loader)
